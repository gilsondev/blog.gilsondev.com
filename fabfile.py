import os
from fabric import utils
from fabric.api import env, require, run
from fabric.contrib.project import rsync_project
from fabric.operations import sudo

RSYNC_EXCLUDE = [
    '*.db',
    '*.pyc',
    '*.sqlite3',
    '.git*',
    '.sass-cache',
    'media/*',
    'tests/*',
    'static/sass',
    'DS_Store',
    'pylintrc',
    'fabfile.py',
    'config.rb',
    'settings/production.py',
]

env.python_version = 'python2.7'
env.project = 'klauslaube'
env.local_root_path = os.path.abspath(os.path.dirname(__file__))


def _paths():
    env.home = os.path.join(env.root, env.project)
    env.project_root_path = os.path.join(env.home, env.project)
    # Manage file
    env.manage_file = os.path.join(env.project_root_path, 'manage.py')
    # Http paths
    env.static_path = os.path.join(env.root, '%s_static' % env.project)
    env.apache_conf = os.path.join(env.home, 'apache2', 'conf',
        '%s.conf' % env.project)
    env.cgi_file = os.path.join(env.home, 'dispatch.wsgi')
    # Virtualenv paths
    env.virtualenv_root = os.path.join(env.home, 'env')
    env.virtualenv_activate = os.path.join(env.virtualenv_root, 'bin',
        'activate')
    # Requirements path
    env.requirements = os.path.join(env.project_root_path, 'requirements',
        '%s.txt' % env.environment)
    env.post_install_requirements = os.path.join(env.project_root_path,
        'requirements', 'post-install.txt')
    # Search index path
    env.search_index_path = os.path.join(env.project_root_path,
        '%s_index' % env.environment)


def staging():
    """
    Use staging environment on remote host.
    """
    env.environment = 'staging'
    env.python_version = 'python2.6'

    # Remote settings
    env.user = 'klauslaube'
    env.hosts = ['192.168.0.106', ]

    # Paths
    env.root = os.path.join('/home', 'klauslaube', 'webapps')
    _paths()


def production():
    """
    Use production environment on remote host.
    """
    env.environment = 'production'
    env.python_version = 'python2.7'

    # Remote settings
    env.hosts = ['108.59.11.109', ]
    env.user = 'kplaube'

    # Paths
    env.root = os.path.join('/home', 'kplaube', 'webapps')
    _paths()


def create_virtualenv():
    """
    Setup virtualenv on remote host.
    """
    require('virtualenv_root', provided_by=('staging', 'production'))

    run('virtualenv --clear --no-site-packages --distribute --python=%(python_version)s '
        '%(virtualenv_root)s' % env)


def deploy():
    """
    Send the code to the remote host.
    """
    require('home', provided_by=('staging', 'production'))

    # Copy the project
    rsync_project(
        remote_dir=env.home,
        local_dir=env.local_root_path,
        exclude=RSYNC_EXCLUDE,
        delete=True,
        extra_opts='--omit-dir-times',
    )


def update_requirements():
    """
    Update Python dependencies on remote host.
    """
    require('virtualenv_activate', provided_by=('staging', 'production'))

    command = ('source %(virtualenv_activate)s; '
        'pip install -E %(virtualenv_root)s '
        '-r %(_requirements)s; deactivate')

    run(command % {
        'virtualenv_activate': env.virtualenv_activate,
        'virtualenv_root': env.virtualenv_root,
        '_requirements': env.requirements,
    })
    run(command % {
        'virtualenv_activate': env.virtualenv_activate,
        'virtualenv_root': env.virtualenv_root,
        '_requirements': env.post_install_requirements,
    })


def syncdb():
    """
    Execute syncdb on remote host.
    """
    require('virtualenv_activate', provided_by=('staging', 'production'))

    run(('source %(virtualenv_activate)s; python %(manage_file)s '
        ' syncdb --settings=settings.%(environment)s') % env)


def migrate(apps):
    """
    Execute South on remote host, to update the database structure.
    """
    require('manage_file', provided_by=('staging', 'production'))

    commands = [('python %(manage_file)s migrate %(app)s '
            '--settings=settings.%(environment)s') % {
        'manage_file': env.manage_file,
        'app': app,
        'environment': env.environment,
    } for app in apps.split()]

    run('source %(virtualenv_activate)s; %(commands)s; deactivate' % {
        'virtualenv_activate': env.virtualenv_activate,
        'commands': ';'.join(commands),
    })


def update_apache_conf():
    """
    Move apache and fastcgi files to the public html.
    """
    require('apache_conf', provided_by=('staging', 'production'))

    local_apache_path = os.path.join(env.project_root_path, 'apache')
    local_apache_conf = os.path.join(local_apache_path,
        '%s.conf' % env.environment)
    local_apache_cgi = os.path.join(local_apache_path,
        '%s.wsgi' % env.environment)

    commands = []
    if env.environment == 'staging':
        commands.append('cp %(local_apache_conf)s %(apache_conf)s; ' % {
            'local_apache_conf': local_apache_conf,
            'apache_conf': env.apache_conf,
        })

    commands.append(('cp %(local_apache_cgi)s %(cgi_file)s; '
            'chmod 0755 %(cgi_file)s') %{
        'local_apache_cgi': local_apache_cgi,
        'cgi_file': env.cgi_file,
    })

    run(''.join(commands))
    http_restart()


def http_restart():
    """
    Restart the HTTP server on remote server.
    """
    require('environment', provided_by=('staging', 'production'))

    if env.environment == 'staging':
        sudo('/sbin/service httpd restart')
    else:
        run(os.path.join(env.home, 'apache2/bin/restart'))


def database_restart():
    """
    Restart the database server on remote server.
    """
    require('environment', provided_by=('staging', 'production'))

    if env.environment == 'staging':
        sudo('/sbin/service mysqld restart')
    else:
        utils.abort('Not yet implemented in production')


def collect_static():
    """
    Collect all static files from Django apps and copy them into the public
    static folder.
    """
    require('project_root_path', provided_by=('staging', 'production'))

    run(('source %(virtualenv_activate)s; '
            'python %(manage_file)s collectstatic %(settings_condition)s; '
            'python %(manage_file)s compress %(settings_condition)s; '
            'deactivate') % {
        'virtualenv_activate': env.virtualenv_activate,
        'manage_file': env.manage_file,
        'settings_condition': '--settings=settings.%s' % env.environment,
    })


def bootstrap():
    """
    Initialize remote host environment.
    """
    require('root', provided_by=('staging', 'production'))

    # Create virtualenv to wrap the environment
    create_virtualenv()
    # Send the project to the remote host
    deploy()
    # Install dependencies on the virtualenv
    update_requirements()
    # Create the database
    syncdb()
