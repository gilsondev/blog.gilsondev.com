--requirement=base.txt

coverage==3.5.1
django-coverage==1.2.1
django-debug-toolbar==0.8.5
pep8==0.6.1
