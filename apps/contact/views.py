# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import FormView

from django.contrib import messages

from contact.forms import ContactForm


class ContactView(FormView):
    form_class = ContactForm
    template_name = 'contact/contact_form.html'

    def form_valid(self, form):
        form.send()
        messages.success(self.request,
            _(u'Sua mensagem foi enviada com sucesso! Obrigado.'))

        return super(ContactView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request,
            _(u'Ops! Você tem certeza que o formulário foi preenchido '
                'corretamente?'))

        return super(ContactView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('contact:contact')
