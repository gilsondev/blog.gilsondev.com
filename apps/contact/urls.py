from django.conf.urls.defaults import patterns, include, url
from contact.views import ContactView

urlpatterns = patterns('',
    url(r'^$', ContactView.as_view(), name='contact'),
)
