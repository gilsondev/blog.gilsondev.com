# -*- coding: utf-8 -*-

from django.core import mail
from django import forms
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from contact import settings


class ContactForm(forms.Form):
    name = forms.CharField(label=_('Nome'), max_length=100)
    email = forms.EmailField(label=_('E-mail'))
    subject = forms.CharField(label=_('Assunto'), max_length=255)
    message = forms.CharField(
        label=_('Mensagem'),
        widget=forms.Textarea(),
    )

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)

        # Settings HTML5 attributes to fields
        self.fields['name'].widget.attrs.update({
            'autofocus': 'true',
            'placeholder': _('Seu nome...'),
        })
        self.fields['email'].widget.input_type = 'email'
        self.fields['email'].widget.attrs.update({
            'placeholder': _('seu-email@provedor.com.br'),
        })
        self.fields['subject'].widget.attrs.update({
            'placeholder': _(u'O título do seu e-mail'),
        })
        self.fields['message'].widget.attrs.update({
            'placeholder': _('Sua mensagem...'),
        })

        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'required': 'true',
            })

    def send(self, to_email=settings.SITE_EMAIL):
        message = render_to_string('contact/contact_email.html',
            self.cleaned_data)

        return mail.send_mail(
            self.cleaned_data['subject'],
            self.cleaned_data['message'],
            self.cleaned_data['email'],
            [to_email, ],
            fail_silently=True,
        )
