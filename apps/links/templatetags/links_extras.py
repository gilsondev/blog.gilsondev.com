from django.template import Library
from links.models import Category, Link

register = Library()


@register.inclusion_tag('templatetags/link_list.html')
def link_list(slug=None):
    context = {
        'category': None,
        'links': None,
    }

    if slug:
        try:
            category = Category.objects.get(slug=slug)
        except Category.DoesNotExist:
            return context

        context['category'] = category
        context['links'] = Link.actives.filter(category=category).all()
    else:
        context['links'] = Link.actives.all()

    return context
