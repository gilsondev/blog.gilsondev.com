from django.db import models
from core.managers import ActiveManager


class LinkActiveManager(ActiveManager):
    def get_query_set(self, *args, **kwargs):
        qs = super(LinkActiveManager, self).get_query_set(*args, **kwargs)
        return qs.filter(category__is_active=True)
