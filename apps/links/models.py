# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext as __

from links import managers


class Category(models.Model):
    title = models.CharField(_(u'Título'), max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    is_active = models.BooleanField(_('Ativo'), default=True)

    objects = models.Manager()
    actives = managers.ActiveManager()

    class Meta:
        verbose_name = __('Categoria')
        verbose_name_plural = __('Categorias')
        ordering = ('title', )

    def __unicode__(self):
        return self.title


class Link(models.Model):
    category = models.ForeignKey(Category, verbose_name=_('Categoria'))
    title = models.CharField(_(u'Título'), max_length=100)
    description = models.TextField(_(u'Descrição'), blank=True, null=True)
    href = models.URLField(_(u'Endereço'), verify_exists=False)
    is_active = models.BooleanField(_('Ativo'), default=True)

    objects = models.Manager()
    actives = managers.LinkActiveManager()

    class Meta:
        verbose_name = __('Link')
        verbose_name_plural = __('Links')
        ordering = ('category__title', 'title', )

    def __unicode__(self):
        return self.title
