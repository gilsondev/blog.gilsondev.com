from django.test import TestCase
from links.models import Link
from links.templatetags.links_extras import link_list


class LinksTemplatetagsTests(TestCase):
    fixtures = ['links_testdata.json', ]

    def test_link_list(self):
        # All links
        context = link_list()
        self.assertEquals(len(context['links']), 3)

        # Links by category
        link = Link.objects.get(pk=4)
        link.category.is_active = True
        link.is_active = True
        link.save()

        context = link_list(slug=link.category.slug)
        self.assertEquals(len(context['links']), 1)
        self.assertEquals(context['links'][0], link)
