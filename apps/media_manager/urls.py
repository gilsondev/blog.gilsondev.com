from django.conf.urls.defaults import patterns, url

from django.contrib.auth.decorators import login_required

from media_manager.views import MediaAddView, MediaDeleteView, MediaListView

urlpatterns = patterns('',
    url(r'^$', login_required(MediaListView.as_view()), name='media_list'),
    url(r'^add/$', login_required(MediaAddView.as_view()), name='media_add'),
    url(r'^(?P<code>[\w=]+)/delete/$', login_required(MediaDeleteView.as_view()),
        name='media_delete'),
)
