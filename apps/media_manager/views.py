from base64 import b64decode
import os

from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView
from django.views.generic.edit import DeletionMixin, FormView

from django.contrib import messages

from media_manager.forms import ImageUploadForm
from media_manager.utils import MediaTree


class MediaBaseView(TemplateView):
    def __init__(self, *args, **kwargs):
        super(MediaBaseView, self).__init__(*args, **kwargs)
        self.tree = MediaTree()


class MediaListView(MediaBaseView):
    template_name = 'media_manager/admin/media_list.html'

    def get_context_data(self, **kwargs):
        return {
            'media_list': self.tree.files,
        }


class MediaAddView(FormView):
    form_class = ImageUploadForm
    template_name = 'media_manager/admin/media_add.html'

    def get_form_kwargs(self):
        kwargs = super(MediaAddView, self).get_form_kwargs()
        self.tree = MediaTree()
        kwargs.update({
            'tree': MediaTree().paths,
        })

        return kwargs

    def get_success_url(self):
        return reverse('media_manager:media_list')

    def form_valid(self, form):
        form.upload()
        messages.success(self.request,
            _(u'Image uploaded.'))

        return super(MediaAddView, self).form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request,
            _(u'An error occurred while trying to send the form.'))

        return super(MediaAddView, self).form_invalid(form)

class MediaDeleteView(MediaBaseView, DeletionMixin):
    template_name = 'media_manager/admin/media_delete.html'

    def get_context_data(self, **kwargs):
        try:
            kwargs.update({
                'file': self.tree.file_infos(b64decode(kwargs['code'])),
            })
        except:
            raise Http404

        return kwargs

    def delete(self, request, code, *args, **kwargs):
        file_ = self.tree.file_infos(b64decode(code))
        os.remove(file_['path'])
        messages.success(request,
            _('Image removed.'))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('media_manager:media_list')
