from PIL import Image
import os

from django.conf import settings

from media_manager.forms import PATHS, ImageUploadForm
from media_manager.tests.base import MediaManagerBaseTests
from media_manager.utils import MediaTree


class ImageUploadFormTests(MediaManagerBaseTests):
    def setUp(self):
        super(ImageUploadFormTests, self).setUp()

        self.tree = MediaTree()

    def test_upload_image(self):
        form = ImageUploadForm(self.data, self.data_file, tree=self.tree.tree)
        self.assertTrue(form.is_valid())
        form.upload()

        self.assertTrue(os.path.exists(self.uploaded_image_path))

    def test_upload_to_media_path(self):
        data = {'upload_to': '/'}
        uploaded_image_path = os.path.join(settings.MEDIA_ROOT, 'testimage.jpg')

        form = ImageUploadForm(data, self.data_file, tree=self.tree.tree)
        self.assertTrue(form.is_valid())
        form.upload()

        self.assertTrue(os.path.exists(uploaded_image_path))

    def test_resize_image(self):
        self.data.update({
            'manipulation': 'resize',
            'width': 250,
            'height': 250,
        })
        form = ImageUploadForm(self.data, self.data_file, tree=self.tree.tree)
        self.assertTrue(form.is_valid())
        form.upload()

        image = Image.open(self.uploaded_image_path)
        size = image.size
        self.assertTrue(os.path.exists(self.uploaded_image_path))
        self.assertEquals(size, (250, 250))
