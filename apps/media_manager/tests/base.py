from base64 import b64encode
from datetime import datetime
import os
import shutil

from django.core.files.uploadedfile import SimpleUploadedFile
from django.conf import settings
from django.test import TestCase


class MediaManagerBaseTests(TestCase):
    MEDIA_ROOT = os.path.join(settings.PROJECT_ROOT_PATH, 'media_tests')

    def setUp(self):
        # Prevent creation of folders
        self._remove_media_tests_folder()

        test_paths = ('first_path', 'second_path', 'third_path', 'fourth_path', )
        self.mocked_path = os.path.join(self.MEDIA_ROOT, 'media_manager_mock')
        self.mocked_url = os.path.join(settings.MEDIA_URL, 'media_manager_mock/')

        self.paths = [self.MEDIA_ROOT, self.mocked_path, ]
        self.urls = [settings.MEDIA_URL, self.mocked_url, ]

        for path in test_paths:
            i = os.path.join(self.mocked_path, path)
            self.paths.append(i)
            self.urls.append('%s/' % os.path.join(self.mocked_url, path))

        # Create paths to tests
        for i in self.paths:
            os.mkdir(i)

        inside_path = os.path.join(self.paths[5], 'inside_path')
        inside_url = os.path.join(self.urls[5], 'inside_path/')
        inside_path_file = os.path.join(inside_path, 'test.jpg')
        inside_url_file = os.path.join(inside_url, 'test.jpg')

        self.paths.append(inside_path)
        os.mkdir(inside_path)
        self.paths.append(inside_path_file)
        f = open(inside_path_file, 'w')
        f.write('Lorem ipsum...')
        f.close()

        self.urls.append(inside_url)
        self.urls.append(inside_url_file)
        self.files = [{
            'url': inside_url_file,
            'path': inside_path_file,
            'folder': '%s/' % os.path.split(inside_url_file)[0],
            'file': os.path.split(inside_url_file)[-1],
            'date': datetime.fromtimestamp(os.path.getmtime(inside_path_file)),
            'size': '0.01 KB',
            'code': b64encode(inside_url_file),
        }, ]

        # Order by name
        self.paths.sort()
        self.urls.sort()

        # Default data to form tests
        self.image_to_upload = os.path.join(settings.PROJECT_ROOT_PATH,
            'apps', 'media_manager', 'fixtures', 'testimage.jpg')
        self.image = open(self.image_to_upload, 'rb')
        self.data = {'upload_to': 'media_manager_mock/first_path/'}
        self.data_file = {
            'image': SimpleUploadedFile(self.image.name, self.image.read())
        }
        self.uploaded_image_path = os.path.join(settings.MEDIA_ROOT,
            'media_manager_mock/first_path/testimage.jpg')

    def tearDown(self):
        self._remove_media_tests_folder()

    def _remove_media_tests_folder(self):
        try:
            shutil.rmtree(self.MEDIA_ROOT)
        except OSError:
            pass
