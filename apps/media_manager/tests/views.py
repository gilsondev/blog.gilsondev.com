from base64 import b64encode
import os

from django.conf import settings
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from media_manager.tests.base import MediaManagerBaseTests


class MediaManagerViewsTests(MediaManagerBaseTests):
    def setUp(self):
        super(MediaManagerViewsTests, self).setUp()

        # Create an user
        self.user = User.objects.create(username='test')
        self.user.set_password('test')
        self.user.save()

        # Authenticate the user
        self.client.login(username='test', password='test')

    def test_list_media(self):
        response = self.client.get(reverse('media_manager:media_list'))
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.context['media_list'], self.files)

    def test_delete_media(self):
        url = reverse('media_manager:media_delete',
            kwargs={'code': b64encode(self.files[0]['url'])})

        # Get method
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.context['file']['url'],
            self.files[0]['url'])

        # Post method
        response = self.client.post(url)
        self.assertEquals(response.status_code, 302)
        self.assertFalse(os.path.exists(self.files[0]['path']))

    def test_add_media(self):
        url = reverse('media_manager:media_add')

        # Get method
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

        # Invalid form post
        response = self.client.post(url)
        self.assertEquals(response.status_code, 200)

        # Post method
        response = self.client.post(url, {
            'upload_to': self.data['upload_to'],
            'image': open(self.image_to_upload),
        })
        self.assertEquals(response.status_code, 302)
