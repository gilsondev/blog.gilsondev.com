from PIL import Image
import os

from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

PATHS = [('/', 'media'), ]

MANIPULATIONS = (
    ('', _(u'---')),
    ('resize', _(u'Resize')),
)


class ImageUploadForm(forms.Form):
    upload_to = forms.ChoiceField(
        label=_(u'Upload to'),
        choices=PATHS,
        initial=[0][0],
    )
    image = forms.ImageField(label=_(u'Image'))
    manipulation = forms.ChoiceField(
        label=_(u'Image manipulations'),
        choices=MANIPULATIONS,
        required=False,
    )

    width = forms.IntegerField(label=_(u'Width'), required=False)
    height = forms.IntegerField(label=_(u'Height'), required=False)

    def __init__(self, *args, **kwargs):
        tree = None

        # Filled up the upload_to field
        if 'tree' in kwargs:
            tree = kwargs['tree']
            del kwargs['tree']

        super(ImageUploadForm, self).__init__(*args, **kwargs)

        if tree:
            self._fill_upload_to_field(tree)

    def _fill_upload_to_field(self, tree):
        paths = []

        for path in tree:
            path = path.replace(settings.MEDIA_ROOT, '')
            indx = path

            if indx.startswith('/'):
                indx = indx[1:]
            if not indx.endswith('/'):
                indx = '%s/' % indx

            paths.append((indx, indx))

        paths[0] = ('/', 'media/')
        self.fields['upload_to'].choices = paths

    def _save_image(self, image, file_path):
        handler = open(file_path, 'w')
        handler.write(image.read())
        handler.close()

    def _resize_image(self, file_path, width, height):
        image = Image.open(file_path).resize((width, height), Image.ANTIALIAS)
        image.save(file_path)

    def upload(self, *args, **kwargs):
        cdata = self.cleaned_data

        if cdata['upload_to'].startswith('/'):
            cdata['upload_to'] = cdata['upload_to'][1:]

        file_path = os.path.join(settings.MEDIA_ROOT, cdata['upload_to'],
            cdata['image'].name)
        # Save image
        self._save_image(cdata['image'], file_path)

        # Resize image
        if cdata['manipulation'] == 'resize':
            self._resize_image(file_path, cdata['width'],
                cdata['height'])
