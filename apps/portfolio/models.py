# -*- coding: utf-8 -*-

from datetime import datetime
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from diario.utils import markuping

from portfolio.managers import ActiveManager, JobActiveManager
from portfolio import settings

if settings.HAS_TAG_SUPPORT:
    from tagging.fields import TagField

if settings.HAS_IMAGEKIT_SUPPORT:
    from imagekit.models import ImageSpec
    from imagekit.processors import resize


class Customer(models.Model):
    name = models.CharField(_('Nome'), max_length=100)
    description = models.TextField(_(u'Descrição'), blank=True, null=True)
    website = models.URLField(
        _('Website'),
        verify_exists=False,
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(_('Ativo'), default=True)
    slug = models.SlugField(max_length=100, unique=True)

    objects = models.Manager()
    actives = ActiveManager()

    class Meta:
        verbose_name = _('Cliente')
        verbose_name_plural = _('Clientes')
        ordering = ('name', )

    def __unicode__(self):
        return self.name


class Job(models.Model):
    # Description
    customer = models.ForeignKey(
        Customer,
        verbose_name=_(u'Cliente'),
        blank=True,
        null=True,
    )
    description = models.CharField(_(u'Descrição'), max_length=100)
    details_source = models.TextField(
        _(u'Detalhes'),
        blank=True,
        null=True,
        help_text=_('Escreva utilizando a sintaxe textile'),
    )
    details = models.TextField(_(u'Detalhes em HTML'), blank=True, null=True)
    href = models.URLField(
        _(u'Endereço'),
        verify_exists=False,
        blank=True,
        null=True,
    )
    # Professional description
    my_role = models.CharField(_(u'Meu papel'), max_length=100, blank=True,
        null=True)
    working_regime = models.CharField(
        _(u'Regime de trabalho'),
        max_length=100,
        blank=True,
        null=True,
    )
    i_used_the = models.CharField(
        _(u'O que utilizei'),
        max_length=100,
        blank=True,
        null=True,
    )

    # Image
    sample_image = models.ImageField(
        upload_to='portfolio',
        verbose_name=_(u'Demonstração'),
        blank=True,
        null=True,
    )

    if settings.HAS_IMAGEKIT_SUPPORT:
        thumbnail = ImageSpec(
            [resize.Crop(settings.THUMB_WIDTH, settings.THUMB_HEIGHT), ],
            format='JPEG',
            image_field='sample_image',
        )
        formatted_image = ImageSpec(
            [resize.Fit(settings.FORMATTED_WIDTH,
                settings.FORMATTED_HEIGHT), ],
            format='JPEG',
            image_field='sample_image',
        )

    # Meta
    creation_date = models.DateField(_(u'Data criação'), default=datetime.now)
    is_active = models.BooleanField(_('Ativo'), default=True)
    slug = models.SlugField(max_length=100, unique=True)

    if settings.HAS_TAG_SUPPORT:
        tags = TagField(blank=True)

    objects = models.Manager()
    actives = JobActiveManager()

    class Meta:
        verbose_name = _('Trabalho')
        verbose_name_plural = _('Trabalhos')
        ordering = ('-creation_date', )

    def __unicode__(self):
        return self.description

    def save(self, *args, **kwargs):
        self.details = markuping('textile', self.details_source)
        super(Job, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('portfolio:job_detail', kwargs={
            'slug': self.slug,
        })
