from django.conf import settings

# Image manipulation
HAS_IMAGEKIT_SUPPORT = getattr(settings, 'HAS_IMAGEKIT_SUPPORT', False)
FORMATTED_HEIGHT = None
FORMATTED_WIDTH = 580
THUMB_HEIGHT = 100
THUMB_WIDTH = 560

# Tags
HAS_TAG_SUPPORT = getattr(settings, 'HAS_TAG_SUPPORT', False)

# Pagination
ROWS_PER_PAGE = getattr(settings, 'ROWS_PER_PAGE', 5)
