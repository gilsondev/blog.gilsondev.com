from django.conf.urls.defaults import patterns, url
from portfolio.views import JobDetailView, JobListView

urlpatterns = patterns('',
    url(r'^$', JobListView.as_view(), name='job_list'),
    url(r'^(?P<slug>[\w_-]+)/$', JobDetailView.as_view(), name='job_detail'),
)
