from haystack import indexes, site
from portfolio.models import Job


class JobIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    pub_date = indexes.DateField(model_attr='creation_date')
    i_used_the = indexes.CharField(model_attr='i_used_the')

    def index_queryset(self):
        return Job.actives


site.register(Job, JobIndex)
