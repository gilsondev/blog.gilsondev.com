from django.views.generic import DetailView, ListView
from portfolio.models import Job
from portfolio import settings


class JobListView(ListView):
    model = Job
    paginated_by = settings.ROWS_PER_PAGE
    queryset = Job.actives.all()


class JobDetailView(DetailView):
    model = Job
    context_object_name = 'job'
    queryset = Job.actives.all()
