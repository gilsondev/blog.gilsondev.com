from django.db import models
from core.managers import ActiveManager


class JobActiveManager(ActiveManager):
    def get_query_set(self, *args, **kwargs):
        qs = super(JobActiveManager, self).get_query_set(*args, **kwargs)
        return qs.filter(customer__is_active=True)
