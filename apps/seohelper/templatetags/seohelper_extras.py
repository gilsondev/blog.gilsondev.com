import re
from django.template import Library

from seohelper.models import Document, ObjectMeta
from seohelper import settings

register = Library()


@register.inclusion_tag('templatetags/seo_header.html')
def seo_header(url, object_detail=None, base_title=settings.SITE_NAME):
    m = re.search('^(?P<url>[\w_\-\.\:\/]+)\?(.*)', url)
    if m:
        url = m.groups()[0]

    try:
        meta = Document.objects.get(url=url)
    except Document.DoesNotExist:
        meta = ObjectMeta(object_detail=object_detail)

    return {
        'base_title': base_title,
        'meta': meta,
    }


@register.inclusion_tag('templatetags/seo_header.html')
def seo_custom_header(title=None, description=None, keywords=None, robots=None,
        base_title=settings.SITE_NAME):
    headers = {
        'title': title,
        'description': description,
        'keywords': keywords,
    }

    if robots:
        headers['robot_tags'] = robots

    meta = ObjectMeta(headers)

    return {
        'base_title': base_title,
        'meta': meta,
    }
