from django.test import TestCase

from django.contrib.flatpages.models import FlatPage

from seohelper.models import Document
from seohelper.templatetags.seohelper_extras import seo_header, \
    seo_custom_header


class SeohelperTemplatetagsTests(TestCase):
    fixtures = ['seohelper_testdata.json', ]

    def test_seo_header(self):
        url = '/'
        document = Document.objects.get(url=url)

        # Test without object
        content = seo_header(url + '?q=1')
        self.assertEquals(content['meta'], document)

        # Test with object
        flatpage = FlatPage.objects.get(pk=1)
        content = seo_header(url + '/fake-url/', flatpage)
        self.assertEquals(content['meta'].title, flatpage.title)

    def test_seo_custom_header(self):
        content = seo_custom_header(
            title='Custom title',
            description='Custom description',
            keywords='my, custom, keywords',
        )
        self.assertEquals(content['meta'].title, 'Custom title')
        self.assertEquals(content['meta'].robot_tags, 'noindex,nofollow')
