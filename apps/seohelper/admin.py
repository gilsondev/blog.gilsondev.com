# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from diario.admin import EntryAdmin
from diario.models import Entry
from seohelper.models import Document, ROBOT_TAGS


class EntryForm(forms.ModelForm):
    seo_title = forms.CharField(
        label=_(u'Título'),
        max_length=255,
        required=False,
    )
    seo_description = forms.CharField(
        label=_(u'Descrição'),
        widget=forms.Textarea(),
        required=False,
    )
    seo_keywords = forms.CharField(
        label=_(u'Keywords'),
        max_length=255,
        required=False,
    )
    seo_robots = forms.ChoiceField(
        choices=ROBOT_TAGS,
        initial=ROBOT_TAGS[0][0],
        required=False,
    )

    class Meta:
        model = Entry

    def __init__(self, *args, **kwargs):
        super(EntryForm, self).__init__(*args, **kwargs)

        fieldsets = list(EntryAdmin.fieldsets)
        fieldsets.insert(2, (_('SEO'), {
            'fields': ('seo_title', 'seo_description', 'seo_keywords',
                'seo_robots', ),
        }))
        EntryAdmin.fieldsets = fieldsets



class EntryWithSeoAdmin(EntryAdmin):
    form = EntryForm


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', )
    search_fields = ('title', 'description', )


#admin.site.unregister(Entry)
admin.site.register(Document, DocumentAdmin)
#admin.site.register(Entry, EntryWithSeoAdmin)
