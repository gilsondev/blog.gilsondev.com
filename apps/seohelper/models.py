# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

ROBOT_TAGS = (
    ('index,follow', 'Index, Follow'),
    ('noindex,follow', 'No index, Follow'),
    ('index,nofollow', 'Index, No follow'),
    ('noindex,nofollow', 'No index, No follow'),
)


class ObjectMeta(object):
    default_robot_tag = ROBOT_TAGS[3][0]

    def __init__(self, object_detail):
        if type(object_detail) == dict:
            get_value = self._get_value_from_dict
        else:
            get_value = self._get_value_from_object

        self.title = get_value(object_detail, 'title')
        self.description = get_value(object_detail, 'description')
        self.keywords = get_value(object_detail, 'keywords')
        self.robot_tags = get_value(object_detail, 'robot_tags',
            self.default_robot_tag)

    def _get_value_from_object(self, object_, index, default_value=None):
        return getattr(object_, index, default_value)

    def _get_value_from_dict(self, dict_, index, default_value=None):
        return dict_.get(index, default_value)


class Document(models.Model):
    url = models.CharField(_(u'URL'), max_length=255)
    title = models.CharField(_(u'Título'), max_length=140)
    description = models.TextField(_(u'Descrição'), blank=True, null=True)
    keywords = models.CharField(
        _(u'Palavras-chave'),
        max_length=255,
        blank=True,
        null=True,
    )
    robot_tags = models.CharField(
        _(u'Robots'),
        max_length=20,
        choices=ROBOT_TAGS,
        default=ROBOT_TAGS[0][0],
    )

    class Meta:
        verbose_name = _('Documento')
        verbose_name_plural = _('Documentos')
        ordering = ('url', )

    def __unicode__(self):
        return self.title
