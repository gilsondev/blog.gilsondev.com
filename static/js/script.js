/* Author: 

*/

;(function (dojo, window, document, undefined) {
    
    window.App = {
        'init': function() {
            dojo.require('dojox.fx.scroll');

            dojo.query('a[href^="#"]').connect('onclick', function(e) {
                e.preventDefault();

                var id = /#([\w_-]+)$/.exec(e.target.href)[1];
                dojox.fx.smoothScroll({
                    'node': dojo.byId(id),
                    'win': window
                }).play();
            });
        },

        'Entry': function() {
            var postDetail  = dojo.query('.post-detail')[0];

            dojo.query('a[href="#respond"]', postDetail).connect('onclick',
                    function(e) {
                dojo.query('#id_name', postDetail)[0].focus();
            });
        }
    };
    window.App.init();

})(dojo, window, document);
