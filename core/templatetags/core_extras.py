import re
from django.template import Library

register = Library()

SPLIT_PATTERN = '<!-- readmore -->'
OPENED_ELEMENTS_PATTERN = '\<\w+\>\s*$'


@register.simple_tag
def current(path, pattern, klass="current"):
    if re.search(pattern, path):
        return klass

    return ''


@register.filter(name='readmore')
def readmore(content):
    if SPLIT_PATTERN in content:
        content = content.split('<!-- readmore -->')[0]
        # Check for opened elements
        while re.search(OPENED_ELEMENTS_PATTERN, content):
            content = re.sub(OPENED_ELEMENTS_PATTERN, '', content)

    return content
