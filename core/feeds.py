from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.contrib.sites.models import Site
from django.contrib.syndication.views import Feed, FeedDoesNotExist

from diario.models import Entry
from tagging.models import Tag

class DiarioRssEntriesFeed(Feed):
    description = _('Pythonista, Djangonauta e desenvolvedor Web')

    def title(self):
        site = Site.objects.get_current()
        return _('%(title)s Blog') % {
            'title': site.name,
        }

    def link(self):
        return reverse('diario-entry-list')

    def get_query_set(self):
        return Entry.published_on_site.order_by('-pub_date')

    def items(self):
        return self.get_query_set()[:10]

    def item_author_name(self, entry):
        if entry.author.get_full_name():
            return entry.author.get_full_name()
        return entry.author.username

    def item_author_email(self, entry):
        return entry.author.email

    def item_pubdate(self, entry):
        return entry.pub_date

    def item_categories(self, entry):
        try:
            return entry.tags.split()
        except AttributeError:
            pass


class DiarioRssEntriesByTagFeed(Feed):
    def get_object(self, request, tag_name):
        return Tag.objects.get(name__exact=tag_name)

    def title(self, tag):
        site = Site.objects.get_current()
        return _('%(title)s Blog: %(tag_name)s') % {
            'title': site.name,
            'tag_name': tag.name,
        }

    def description(self, tag):
        return _('Lastest entries for tag %(tag_name)s') % {
            'tag_name': tag.name,
        }

    def link(self, tag):
        if not tag:
            raise FeedDoesNotExist
        return reverse('diario.views.tagged.tagged_entry_list', kwargs={
            'tag': tag.name,
        })
        return ''

    def get_query_set(self, tag):
        return (Entry.published_on_site.filter(tags__contains=tag.name)
                    .order_by('-pub_date'))

    def items(self, tag):
        return self.get_query_set(tag)[:10]
