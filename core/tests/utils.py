from django.test import TestCase

from core.utils import slideshare_shortcode


class CoreUtilsTests(TestCase):
    slideshare_shortcodes = (
        ('[slideshare id=8354309'
            '&doc=canvasvssvgdavidsonfellipe-110619115605-phpapp02]'),
        '[slideshare id=7916312&doc=font-face-verdana-110510185728-phpapp02]',
        '[slideshare id=8346961&doc=frontinrio-110618122909-phpapp02]',
    )

    def test_slideshare_shortcode(self):
        # Test one shortcode
        expected = ('<iframe '
            'src="http://www.slideshare.net/slideshow/embed_code/8354309" '
            'width="590" height="481" frameborder="0" marginwidth="0" '
            'marginheight="0" scrolling="no"></iframe>')
        response = slideshare_shortcode(self.slideshare_shortcodes[0])
        self.assertEquals(response, expected)

        # Test multiple shortcodes
        content = ('<p>See below!</p>%s<p>Another one!</p>%s'
            '<h2>Now, with header!</h2>%s') % self.slideshare_shortcodes
        expected = ('<p>See below!</p>'
            '<iframe '
            'src="http://www.slideshare.net/slideshow/embed_code/8354309" '
            'width="590" height="481" frameborder="0" marginwidth="0" '
            'marginheight="0" scrolling="no"></iframe>'
            '<p>Another one!</p>'
            '<iframe '
            'src="http://www.slideshare.net/slideshow/embed_code/7916312" '
            'width="590" height="481" frameborder="0" marginwidth="0" '
            'marginheight="0" scrolling="no"></iframe>'
            '<h2>Now, with header!</h2>'
            '<iframe '
            'src="http://www.slideshare.net/slideshow/embed_code/8346961" '
            'width="590" height="481" frameborder="0" marginwidth="0" '
            'marginheight="0" scrolling="no"></iframe>'
        )
        response = slideshare_shortcode(content)
        self.assertEquals(response, expected)
