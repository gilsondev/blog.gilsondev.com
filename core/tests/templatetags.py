from django.test import TestCase
from django.test.client import RequestFactory

from core.templatetags.core_extras import current, readmore


class CoreTemplateTagsTests(TestCase):
    def test_current_link(self):
        factory = RequestFactory()
        request = factory.get("/blog/")

        class_ = current(request.path, "^/blog/$")
        self.assertEquals(class_, "current")

    def test_readmore(self):
        paragraphs = (
            '<p>First Paragraph!</p>',
            '<section><div><p><!-- readmore --></p></div></section>',
            '<p>Second Paragraph!</p>',
        )
        content = ''.join(paragraphs)
        response = readmore(content)

        self.assertEquals(response, paragraphs[0])
