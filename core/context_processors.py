from sys import version_info as python_version
from django import VERSION as django_version
from diario import VERSION as diario_version
from django.conf import settings


def project_versions(request):
    return {
        'PYTHON_VERSION': '%s.%s.%s' % (python_version[0], python_version[1],
            python_version[2]),
        'DJANGO_VERSION': '%s.%s.%s' % (django_version[0], django_version[1],
            django_version[2]),
        'DIARIO_VERSION': '%s.%s.%s' % (diario_version[0], diario_version[1],
            diario_version[2]),
    }


def project_settings(request):
    return {
        'GOOGLE_ANALYTICS_KEY': settings.GOOGLE_ANALYTICS_KEY,
    }
