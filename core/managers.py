from django.db import models


class ActiveManager(models.Manager):
    def get_query_set(self, *args, **kwargs):
        qs = super(ActiveManager, self).get_query_set(*args, **kwargs)
        return qs.filter(is_active=True)
