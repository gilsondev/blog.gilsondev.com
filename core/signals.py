from django.contrib.comments.moderation import CommentModerator
from core.utils import slideshare_shortcode


class EntryModerator(CommentModerator):
    email_notification = True
    enable_field = 'enable_comments'


def replace_slideshare_shortcode(instance, **kwargs):
    instance.body = slideshare_shortcode(instance.body)
