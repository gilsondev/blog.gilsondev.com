import re

def slideshare_shortcode(content):
    pattern = r'\[slideshare\s+id=(?P<id>\d+)&[\w\s\&\=\_\-]+\]'
    slideshare_frame = ('<iframe src="http://www.slideshare.net/slideshow'
        '/embed_code/%(id)s" width="590" height="481" frameborder="0" '
        'marginwidth="0" marginheight="0" scrolling="no"></iframe>')
    m = re.search(pattern, content)

    while m:
        parms = m.groupdict()
        sub_pattern = pattern.replace('(?P<id>\d+)', parms['id'])
        content = re.sub(sub_pattern, slideshare_frame % parms, content)

        m = re.search(pattern, content)

    return content
