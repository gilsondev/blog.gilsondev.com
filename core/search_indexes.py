from haystack import indexes, site
from diario.models import Entry


class EntryIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='author')
    pub_date = indexes.DateTimeField(model_attr='pub_date')
    tags = indexes.MultiValueField()

    def index_queryset(self):
        return Entry.published_on_site

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]


site.register(Entry, EntryIndex)
