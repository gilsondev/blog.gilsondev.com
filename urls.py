from django.conf import settings
from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
import haystack

from core.feeds import DiarioRssEntriesFeed, DiarioRssEntriesByTagFeed

# Autodiscover admin and haystack
admin.autodiscover()
haystack.autodiscover()

# Connect signal to diario
from django.db.models import signals
from django.contrib.comments.moderation import CommentModerator, moderator
from diario.models import Entry
from core.signals import replace_slideshare_shortcode, EntryModerator

signals.pre_save.connect(replace_slideshare_shortcode, sender=Entry)
moderator.register(Entry, EntryModerator)

# URL settings
handler500 = 'klauslaube.views.server_error'

urlpatterns = patterns('',
    # Blog feeds
    url(r'^feed/$', DiarioRssEntriesFeed(), name='diario_feed'),
    url(r'^tag/(?P<tag_name>[^/]+)/feed/$', DiarioRssEntriesByTagFeed(),
        name='diario_tag_feed'),

    # Blog urls
    url(r'', include('diario.urls.entries')),
    url(r'^tag/', include('diario.urls.tagged')),
    url(r'^comentarios/', include('django.contrib.comments.urls')),

    url(r'^portfolio/', include('portfolio.urls', namespace='portfolio')),
    url(r'^contato/', include('contact.urls', namespace='contact')),

    # Search urls
    url(r'^busca/', include('haystack.urls')),

    # Admin urls
    url(r'^admin/media_manager/', include('media_manager.urls',
        namespace='media_manager')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT)
